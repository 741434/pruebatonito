import pandas as pd
import matplotlib.pyplot as plt
import plotly
from plotly.subplots import make_subplots
pd.options.plotting.backend = "plotly"
import plotly.graph_objs as go 

##############################################################################################################################################
#####################################################  CONFIGURAR  ###########################################################################
##############################################################################################################################################

ruta_datos_crudo = r"G:\Mi unidad\TFM_2021\Procesado_de_datos\sesiones\sesion4\sesion1\15;37;02-15-06-2021\15;37;02-15-06-2021_sen.csv"

#Para poder tener en el eje x de las gráficas directamente los videos
EJE_X_TIEMPOS_DE_VIDEO = True
PONER_ETIQUETAS  = True
offset_video           = 515.76 #Este es el SEGUNDOS en el cual en el vídeo se aprecia el valor en ms que tiene el microcontrolador en ese momento.
offset_datos           = 58653
offset_video           = 191.36 
offset_datos           = 68625
#cursores = [[4023,"orange"],[1730.8,"blue"],[1736.83,"red"],[1739.2,"green"],[1789.56,"orange"],[1826.400,"blue"],[1831.200,"green"]]#Sesion 2 Howlab3
#cursores = [[3403,"orange"],[3336.83,"blue"],[4980.416,"green"]]#Sesion 2 Howlab2
cursores = [[623.47,"orange","Deja en el suelo HOWLA2"],
            [628.19,"blue","Levanta del suelo la caja"],
            [628.89,"green","Caja a 90 grados"],
            [633.27,"red","Caja enel suelo"],
            [635.8,"black","Caja deja de estar en el suelo"],
            [640.19,"red","Primera agitacion"],
            [641.03,"yellow","Primera agitacion"]
            ]



def suavizar(columna):
    if(True):
        valor_ant2 = 0
        vector_aux2 = []
        peso_valor_Ant = 0.95
        peso_valor_Act = 1-peso_valor_Ant
        for i in range(0, len(columna.values)):
            valor12 = columna.values[i]
            vector_aux2.append(valor12*peso_valor_Act + valor_ant2*peso_valor_Ant)
            valor_ant2 = valor12*peso_valor_Act + valor_ant2*peso_valor_Ant
        columna = pd.DataFrame(vector_aux2) 
        return columna
    else:
        return columna


if(PONER_ETIQUETAS):    
    # FICHERO DE DATOS
    #Ruta con datos del Accel y gyroscopio:
    rutaAcel     = ruta_datos_crudo
    # FICHERO DE TIEMPOS:
    ruta_tiempos_conEtiquetas = r"G:\Mi unidad\TFM_2021\Procesado_de_datos\sesiones\sesion4\sesion1\15;37;02-15-06-2021\videos_unidos_s4.mp4_etiquetas_tiempos_videos_unidos.txt"
    # FICHERO DE SALIDA - Se borrará al final:
    ruta_final_acel    = r"G:\Mi unidad\TFM_2021\Procesado_de_datos\sesiones\sesion3\Acc_etiquetado_borrar.txt"


##############################################################################################################################################
################################################  LEO EL ARCHIVO  ############################################################################
##############################################################################################################################################
df = pd.read_csv(ruta_datos_crudo, ";")

x_values =  df['Timestamp'].unique()
Accel_x  =  df['Accel_x'].tolist()
Accel_y  =  df['Accel_y'].tolist()
Accel_z  =  df['Accel_z'].tolist()

Gyros_x  =  df['Gyro_x'].tolist()
Gyros_y  =  df['Gyro_y'].tolist()
Gyros_z  =  df['Gyro_z'].tolist()

Magne_x  =  df['Magnet_x'].tolist()
Magne_y  =  df['Magnet_y'].tolist()
Magne_z  =  df['Magnet_z'].tolist()

df["Accel_no_suav"] = df['Accel_x']
df['Accel_x'] = suavizar(df['Accel_x'])
df['Accel_y'] = suavizar(df['Accel_y'])
df['Accel_z'] = suavizar(df['Accel_z'])


df = df.assign(Modulo_Acc = (df["Accel_x"]**2 + df["Accel_y"]**2 + df["Accel_z"]**2)**(0.5))
df = df.assign(Modulo_Gyro = (df["Gyro_x"]**2 + df["Gyro_y"]**2 + df["Gyro_z"]**2)**(0.5))
df = df.assign(Modulo_Magne = (df["Magnet_x"]**2 + df["Magnet_y"]**2 + df["Magnet_z"]**2)**(0.5))





df_acc = df
df_acc['Modulo_Acc'] = (df_acc['Modulo_Acc'] - df_acc['Modulo_Acc'].mean())/df_acc['Modulo_Acc'].std(ddof=0)
df_acc['Modulo_Gyro'] = (df_acc['Modulo_Gyro'] - df_acc['Modulo_Gyro'].mean())/df_acc['Modulo_Gyro'].std(ddof=0)

df_acc['Modulo_Acc'] = (df_acc['Modulo_Acc']*10)*(df_acc['Modulo_Acc']*10)
df_acc['Modulo_Gyro'] = (df_acc['Modulo_Gyro']*10)*(df_acc['Modulo_Gyro']*10)



nuevo_vec_df = pd.DataFrame({"Timestamp":x_values,"Modulo_Acc":df["Modulo_Acc"], "Accel_x":df['Accel_x'], "Modulo_Gyro":df["Modulo_Gyro"],"Modulo_Magne":df["Modulo_Magne"]})
nuevo_vec_df.to_csv("uese",sep=';', index=None, decimal='.') 

#Crear Figuras con el propio dataframe
""" figure = df.plot(x = df['Timestamp'], y = df.columns[1:])
figure.show() """

if(EJE_X_TIEMPOS_DE_VIDEO):
    df["Timestamp"] = (df["Timestamp"] - offset_datos)/1000.0 + offset_video


##############################################################################################################################################
################################################  LEEMOS ARCHIVOS DE ETIQUETAS  ##############################################################
##############################################################################################################################################

if(PONER_ETIQUETAS):

    #Ejemplo de uso de estos dos parámetros: Estoy viendo el vídeo y enfoco con la cámara el móvil. Ese es el segundo 512 de vídeo y lo que
    #se está viendo en el móvil es que el numero de ms que lleva el micro encendido son 77s. Con esto offset_video = 512 y offset_datos = 77
    #######################################################
    tipo_delimitador = ';'
    #####################################################################################################################################################################
    f_etiquetas      = open(ruta_tiempos_conEtiquetas,"r") #Fichero generado con la interfaz gráfica. Formato texto: Tiempo Inicio\tActividad\tTiempo Final
    vector_etiquetas = f_etiquetas.readlines()

    #rutas = [rutaAcel, rutaMag, rutaPress]
    #rutas_fin = [ruta_final_acel, ruta_final_mag, ruta_final_press]
    rutas     = [rutaAcel]
    rutas_fin = [ruta_final_acel]

    for k in range(len(rutas)): #Para todos los archivos que tengo de datos:
        f_raw = open(rutas[k],"r") #Abrimos el archivo con datos de un sensor que hemos recopilado
        vector_datos = f_raw.readlines() #sacamos todas las líneas que haya en forma de lista

        g = open(rutas_fin[k],"w") #Abrimos un archivo de escritura sobre el que volcaremos la nueva info.
        g.write("ACTIVIDAD"+tipo_delimitador+ vector_datos[0])  #Escribimos las cabeceras de Acitividad ID y el resto de cabeceras que por defecto tienen los datos
        contador_elemetos_analizados_de_raw = 1

        for j in range(1, len(vector_datos)): #Vamos a ir recorriendo todas y cada una de las líneas
            no_encontrado = False
            #La idea es coger la primera etiqueta de tiempo de inicio y fin de actividad y todos aquellos datos que estén en ese rango 
            # les ponemos en su campo de actividad, la actividad que huebiera en ese tramo.
            for i in range(0,len(vector_etiquetas)): 
                
                cond1 = (float(vector_datos[j].split(tipo_delimitador)[1]) - offset_datos) < (float(vector_etiquetas[i].split('\t')[2]) - offset_video)*1000
                cond2 = (float(vector_datos[j].split(tipo_delimitador)[1]) - offset_datos) >= (float(vector_etiquetas[i].split('\t')[0]) - offset_video)*1000
                if(cond1 and cond2): #Si se cumplen ambas dos condiciones le pongo el atributo de la etiqueta y salgo del for puesto que esa línea
                                    # de datos ya tiene bien puesta su etiqueta.
                    g.write(str(vector_etiquetas[i].split('\t')[1]) + tipo_delimitador + ( vector_datos[j] )) #Escribo en el fichero de salida
                    no_encontrado = False
                    break
                else:
                    no_encontrado = True
            if(no_encontrado):
                g.write("xx" + tipo_delimitador + vector_datos[j]) #En caso de que la etiqueta de tiempo de los datos esté en un rango sin etiqueta
                                                                # de actividad entonces pongo "xx"
    

    g.close() #Cierro el fichero de salida
    f_raw.close()

    df_acc = pd.read_table(ruta_final_acel, sep=';',header=0)


##############################################################################################################################################
#####################################################  PLOTEAMOS  ############################################################################
##############################################################################################################################################

#Crear varias figuras con plotly
fig = make_subplots(rows=3, cols=1, subplot_titles=( "Accelerómetro","Giroscopio"))
# for i in range (2,5):
#     fig.append_trace(go.Scatter(
#                 x=df["Timestamp"],
#                 y=df[df.columns[i]],
#                 name = df.columns[i]
#                 ), row=1, col=1,) 

# for i in range (5,8):                
#     fig.append_trace(go.Scatter(
#                 x=df["Timestamp"],
#                 y=df[df.columns[i]],
#                 name = df.columns[i]
#                 ), row=2, col=1,) 
# for i in range (8,11):                
#     fig.append_trace(go.Scatter(
#                 x=df["Timestamp"],
#                 y=df[df.columns[i]],
#                 name = df.columns[i]
#                 ), row=3, col=1,) 
fig.append_trace(go.Scatter(
                x=df["Timestamp"],
                y=df["Modulo_Acc"],
                name = "Modulo_acc"
                ), row=1, col=1,) 
fig.append_trace(go.Scatter(
                x=df["Timestamp"],
                y=df["Modulo_Gyro"],
                name = "Modulo_Gyro"
                ), row=1, col=1,) 
# fig.append_trace(go.Scatter(
#                 x=df["Timestamp"],
#                 y=df["Modulo_Magne"],
#                 name = "Modulo_Magne"
#                 ), row=3, col=1,) 

for i in range(0, len(cursores)):
    fig.add_vline(x=cursores[i][0], line_width=3, line_dash="dash", line_color=cursores[i][1])
##############################################################################################################################################
#####################################################  PINTAMOS LAS ACTIVIDADES  #############################################################
##############################################################################################################################################

if(PONER_ETIQUETAS):
       df=df_acc
       if(EJE_X_TIEMPOS_DE_VIDEO):
         df["Timestamp"]=(df["Timestamp"]-offset_datos)/1000.0 + offset_video
       actividad_anterior = "xx"
       tiempo_anterior = 0
       colores = {"**":"red", "Parada":"green", "Andando": "pink","Comiendo": "orange","Corriendo":  "grey", "otros": "purple", "hola": "black", "xx":'rgb(203,213,232)',"ppp": "yellow","ooooo": "pink"}
       print("Hola")
       for i in range(0, len(df)):       
              actividad_actual = df.loc[i,"ACTIVIDAD"]
              if (actividad_actual != actividad_anterior):
                     tiempo_actual = df.loc[i,"Timestamp"]
                     fig.add_vrect(
                     x0=tiempo_anterior, x1=tiempo_actual,
                     fillcolor=colores[actividad_anterior], opacity=0.5,
                     layer="below", line_width=0,
                     annotation_text=actividad_anterior, annotation_position="top",row=1,col=1
                     )
                     
                     fig.add_vrect(
                        x0=tiempo_anterior, x1=tiempo_actual,
                        fillcolor=colores[actividad_anterior], opacity=0.5,
                        layer="below", line_width=0,
                        annotation_text=actividad_anterior, annotation_position="top",row=2,col=1
                        )                   

                     actividad_anterior = actividad_actual
                     tiempo_anterior = tiempo_actual

       fig.add_vrect(
                     x0=tiempo_anterior, x1= df.loc[len(df)-1,"Timestamp"],
                     fillcolor=colores[df.loc[len(df)-1,"ACTIVIDAD"]], opacity=0.5,
                     layer="below", line_width=0,
                     annotation_text=df.loc[len(df)-1,"ACTIVIDAD"], annotation_position="top",row=1,col=1
                     )

       fig.add_vrect(
                     x0=tiempo_anterior, x1= df.loc[len(df)-1,"Timestamp"],
                     fillcolor=colores[df.loc[len(df)-1,"ACTIVIDAD"]], opacity=0.5,
                     layer="below", line_width=0,
                     annotation_text=df.loc[len(df)-1,"ACTIVIDAD"], annotation_position="top",row=2,col=1
                     ) 

fig.show() 
import os
os.remove(ruta_final_acel)



""" Crear figuras con matplotlib """
# fig,ax = plt.subplots(3,1)
# fig.set_size_inches(8,6)
# ax[0].plot(x_values,Accel_x)
# ax[0].plot(x_values,Accel_y)
# ax[0].plot(x_values,Accel_z)

# ax[1].plot(x_values,Gyro_x)
# ax[1].plot(x_values,Gyro_y)
# ax[1].plot(x_values,Gyro_z)

# ax[2].plot(x_values,Magne_x)
# ax[2].plot(x_values,Magne_y)
# ax[2].plot(x_values,Magne_z)

# plt.show()